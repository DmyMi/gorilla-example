package main

import (
	"log"
	"net/http"

	"gitlab.com/dmymi/gorilla-example/routes"
)

func main() {
	router := routes.NewRouter()
	log.Fatal(http.ListenAndServe(":8080", router))
}
