package handlers

type Employee struct {
	Name string `json:"name"`
	Age  uint8  `json:"employee_age"`
}

type Team []Employee
