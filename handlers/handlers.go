package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/go-yaml/yaml"
	"github.com/gorilla/mux"
)

func RootHandler(w http.ResponseWriter, r *http.Request) {
	team := Team{
		Employee{Name: "Vasya", Age: 25},
		Employee{Name: "Petya", Age: 30},
	}
	if err := yaml.NewEncoder(w).Encode(team); err != nil {
		panic(err)
	}
}

func HelloHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	lastname := vars["lastname"]
	var emp Employee
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &emp); err != nil {
		w.WriteHeader(400)
	}

	w.WriteHeader(200)
	fmt.Fprintln(w, "Hello, your name is:", emp.Name, lastname)
}
