package routes

import (
	"net/http"

	"gitlab.com/dmymi/gorilla-example/handlers"

	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routesArray {
		router.Methods(route.Method).Path(route.Pattern).Name(route.Name).Handler(route.HandlerFunc)
	}
	return router
}

var routesArray = []Route{
	Route{
		Name:        "Root",
		Method:      "GET",
		Pattern:     "/",
		HandlerFunc: handlers.RootHandler,
	},
	Route{
		Name:        "Hello",
		Method:      "POST",
		Pattern:     "/hello/{lastname}",
		HandlerFunc: handlers.HelloHandler,
	},
}
